#!/usr/bin/env node

import commander from 'commander'
import App from './index.js'
import oneliner from 'one-liner'
import * as fs from 'fs';

///////////////////////////////////
// Utility functions
///////////////////////////////////
function format(prefix, str)
{
    let time = new Date().toISOString();
    return `${time} ${prefix}: ${oneliner(str)}`;
}
function logTrace(s)
{
    console.log(format('Trace', s));
}
function logError(s)
{
    console.error(format('Error', s));
}
function logDebug(s)
{
    console.log(format('Debug', s));
}

///////////////////////////////////
// Process arguments
///////////////////////////////////
commander.option('-p, --port [number]', 'Port number')
         .option('--s-port [number]', 'Secure port number')
         .option('-f, --file [path]', 'Path to the config file')
         .option('--key [path]', 'Path to the private key file (.key)')
         .option('--cert [path]', 'Path to the certificate file (.pem)')
         .option('-k, --keep-alive [number]', 'The number of milliseconds of inactivity a server needs to wait. Default to 0 which disable the keep alive behavior')
         .parse(process.argv);

let port = commander.port ? commander.port : 8080;
let sPort = commander.sPort ? commander.sPort : 8443;
let configPath = commander.file ? commander.file : 'config.yaml';
let keepAliveTimeout = commander.keepAlive ? commander.keepAlive : (process.env.CONFIGURAPI_RUNNER_SELF_KEEP_ALIVE || 0);
let key = commander.key ? fs.readFileSync(commander.key) : undefined;
let cert = commander.cert ? fs.readFileSync(commander.cert) : undefined;

///////////////////////////////////
// Start the API
///////////////////////////////////
let app = new App();
app.on('error', (s) => {logError(s);});
app.on('debug', (s) => {logDebug(s);});
app.on('trace', (s) => {logTrace(s);});
await app.run({port: port, configPath:configPath, keepAliveTimeout:keepAliveTimeout, key: key, cert: cert, sPort: sPort});