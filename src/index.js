require('dotenv').config();
const http = require('http');
const https = require('https');
const HttpAdapter = require('./httpAdapter');
const events = require('events');
const Configurapi = require('configurapi');
const { option } = require('commander');

module.exports = class HttpRunner extends events.EventEmitter {
    constructor() {
        super();

        this.port = 8000;
        this.sPort = 8443;
        this.configPath = "config.yaml";
        this.service = undefined;
        this.keepAliveTimeout = 0;
        this.key = undefined;
        this.cert = undefined;
    }

    async run(options) {
        this._handleOptions(options);

        let config = Configurapi.Config.load(this.configPath);

        this.service = new Configurapi.Service(config);
        this.service.on("trace", (s) => this.emit("trace", s));
        this.service.on("debug", (s) => this.emit("debug", s));
        this.service.on("error", (s) => this.emit("error", s));
        await this.service.init();

        if(this.cert && this.key && this.sPort)
        {
            let server = https.createServer({key: this.key, cert: this.cert}, (req, resp) => {
                this._requestListener(req, resp);
            });

            server.keepAliveTimeout = this.keepAliveTimeout;
            server.listen(this.sPort);

            this.emit("trace", "Secure Server is listening...");
        }

        let server = http.createServer({key: this.key, cert: this.cert}, (req, resp) => {
            this._requestListener(req, resp);
        });

        server.keepAliveTimeout = this.keepAliveTimeout;
        server.listen(this.port);
        
        this.emit("trace", "Server is listening...");
    }

    _handleOptions(options) {
        if (!options) return;

        if ('port' in options) this.port = options.port;
        if ('configPath' in options) this.configPath = options.configPath;
        if ('keepAlive' in options) this.keepAliveTimeout = options.keepAliveTimeout;
        if ('key' in options) this.key = options.key;
        if ('cert' in options) this.cert = options.cert;
        if ('sPort' in options) this.sPort = options.sPort;
    }

    async _requestListener(incomingMessage, serverResponse) {
        try {
            let event = new Configurapi.Event(await HttpAdapter.toRequest(incomingMessage));

            await this.service.process(event);

            HttpAdapter.write(event.response, serverResponse);
        }
        catch (error) {
            let response = new Configurapi.ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

            HttpAdapter.write(response, serverResponse);
        }
    }
};
