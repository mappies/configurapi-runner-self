const Configurapi = require('configurapi');
const http = require('http');
const URL = require('url');

module.exports = {
    write: function(response, serverResponse)
    {
        serverResponse.writeHead(response.statusCode, response.headers);

        let body = '';

        if(response.body instanceof Buffer)
        {
            body = response.body;
        }
        else if(response.body instanceof String)
        {
            body = response.body;
        }
        else if(response.body instanceof Object)
        {
            let jsonReplacer = 'jsonReplacer' in response ? response.jsonReplacer : undefined;

            body = JSON.stringify(response.body, jsonReplacer);
        }
        else if(response.body instanceof Number)
        {
            body = response.body + '';
        } 
        else 
        {
            body = response.body;
        }
        serverResponse.end(body);
    },

    toRequest: async function(incomingMessage)
    {
        let request = new Configurapi.Request();
        
        request.method = incomingMessage.method.toLowerCase();
        request.headers = incomingMessage.headers;

        let url = URL.parse(incomingMessage.url, true);
        let data = [];

        request.payload = undefined;
        request.query = url.query;
        request.path = url.pathname;
        request.pathAndQuery = url.href;

        return new Promise((resolve, reject) => 
        {    
            incomingMessage.on('data', function(chunk) {
                data.push(chunk);
            }).on('end', function() {
                if(data.length <= 0) {
                    request.payload = undefined;
                    return resolve(request);
                }

                request.payload = Buffer.concat(data);

                if(request.payload && 'content-type' in request.headers)
                {
                    let contentType = request.headers['content-type'];

                    if(contentType.startsWith('text/'))
                    {
                        request.payload = request.payload.toString();
                    }
                    else if(contentType.startsWith('application/json'))
                    {
                        try
                        {
                            if(request.payload)
                            {
                                request.payload = JSON.parse(request.payload);  
                            }
                        }
                        catch(err)
                        {
                            reject(err);
                        } 
                    }
                }

                resolve(request);
            }).on('error', function(err) {
                reject(err);
            });
        });
    }
};
